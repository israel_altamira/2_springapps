package com.sample.validation;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

// https://www.mkyong.com/spring-mvc/spring-3-mvc-and-jsr303-valid-example/

public class Customer {

	// By default, if validation failed.

	// @NotEmpty will display �may not be empty�
	// @Range will display �must be between 1 and 150�

	@NotEmpty // make sure name is not empty
	String name;

	@Range(min = 1, max = 150) // age need between 1 and 150
	int age;

	// getter and setter methods
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Customer() {
	}

	@Override
	public String toString() {
		return "Customer [name=" + name + ", age=" + age + "]";
	}
}