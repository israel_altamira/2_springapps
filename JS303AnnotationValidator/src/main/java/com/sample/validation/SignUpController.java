package com.sample.validation;

//https://www.mkyong.com/spring-mvc/spring-3-mvc-and-jsr303-valid-example/

import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/customer")
public class SignUpController {

	// For validation to work, just annotate the �JSR annotated model object�
	// via @Valid. That�s all, other stuff is just a normal Spring MVC form
	// handling.

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String addCustomer(@Valid Customer customer, BindingResult result) {
		
		// nota que no hay una traduccion de los errores de @Valid
		// hacia los mensajes que acarrea BingingResult
		
		if (result.hasErrors()) {
			return "SignUpForm";
		} else {
			return "Done";
		}
	}

	@RequestMapping(method = RequestMethod.GET)
	public String displayCustomerForm(ModelMap model) {
		model.addAttribute("customer", new Customer());
		return "SignUpForm";
	}

}