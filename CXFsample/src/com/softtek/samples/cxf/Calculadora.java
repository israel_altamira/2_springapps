package com.softtek.samples.cxf;

import javax.jws.WebService;

@WebService
public interface Calculadora {
   double suma(double a, double b);
}
