package com.softtek.samples.cxf.impl;

import com.softtek.samples.cxf.Calculadora;

public class CalculadoraImpl implements Calculadora {
	public double suma(double a, double b) {
		return a + b;
	}
}