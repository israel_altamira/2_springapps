package com.softtek.samples.main;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import com.softtek.samples.cxf.Calculadora;

public class Client {
	public static void main(String[] args) throws MalformedURLException {
		URL urlWsdlCalculadora = new URL("http://localhost:8080/Calculadora?wsdl");
		QName nombreServicioCalculadora = new QName("http://impl.cxf.samples.softtek.com/", "CalculadoraImplService");

		Service servicioCalculadora = Service.create(urlWsdlCalculadora, nombreServicioCalculadora);
		Calculadora calculadora = servicioCalculadora.getPort(Calculadora.class);

		System.out.println(String.format("Calling new Service: suma[%s]", calculadora.suma(11.0, 12.0)));
	}

}
