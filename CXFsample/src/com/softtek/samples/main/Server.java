package com.softtek.samples.main;

import com.softtek.samples.cxf.impl.CalculadoraImpl;
import javax.xml.ws.Endpoint;
import org.apache.cxf.jaxws.EndpointImpl;

public class Server {
	public static void main(String[] args) {
		Endpoint endpointCalculadora = EndpointImpl.create(new CalculadoraImpl());
		endpointCalculadora.publish("http://localhost:8080/Calculadora");
	}
}
