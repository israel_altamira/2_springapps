package com.sample.validation;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class Main {

	public static void main(String[] args) {
		
		// Notice that when asking for a specific group to be validated the Default
		// group will not be validated - to validate constraints which are members
		// of the Default group we must add Default class to the list of groups on
		// the validate() method.
		
		System.out.println("Validaciones round 1 --------------");
		Book book = new Book();
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Book>> violations = validator.validate(book);
		for (ConstraintViolation<Book> violation : violations) {
		   System.out.format("\t%s: %s%n",violation.getPropertyPath(), violation.getMessage());
		}
		
		// Validate for the printing phase
		System.out.println("Validaciones round 2 --------------");
		violations = validator.validate(book, Draft.class);
		for (ConstraintViolation<Book> violation : violations) {
		   System.out.format("\t%s: %s%n",violation.getPropertyPath(), violation.getMessage());
		}

		// Validate both the printing and the Draft phases
		System.out.println("Validaciones round 3 --------------");
		violations = validator.validate(book, Draft.class, Printing.class);
		for (ConstraintViolation<Book> violation : violations) {
		   System.out.format("\t%s: %s%n",violation.getPropertyPath(), violation.getMessage());
		}
		
		// ahora agamoslo con grupos
		
	}
	
}
