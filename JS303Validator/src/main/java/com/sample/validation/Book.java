package com.sample.validation;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class Book {

	// Notice that when asking for a specific group to be validated the Default
	// group will not be validated - to validate constraints which are members
	// of the Default group we must add Default class to the list of groups on
	// the validate() method.

	@NotNull(groups = Draft.class) // puede ser un arreglo {Draft.class,Printing.class}
	private String title;

	@NotNull(groups = Draft.class)
	private String author;

	@Min(value = 100, groups = Printing.class)
	private int numOfPages;

	@NotNull(groups = Printing.class)
	private String ISBN;

}