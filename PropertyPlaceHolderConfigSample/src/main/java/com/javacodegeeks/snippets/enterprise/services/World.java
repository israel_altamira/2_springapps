package com.javacodegeeks.snippets.enterprise.services;

public class World {

    private String prefixProp;

    private String suffixProp;

    public String getPrefixProp() {
        return this.prefixProp;
    }

    public String getSuffixProp() {
        return this.suffixProp;
    }

    public void sayGoodbye() {
        System.out.println(this.suffixProp + "!");
    }

    public void sayHello() {
        System.out.println(this.prefixProp + "!");
    }

    public void setPrefixProp(String prefixProp) {
        this.prefixProp = prefixProp;
    }

    public void setSuffixProp(String suffixProp) {
        this.suffixProp = suffixProp;
    }
}
