package com.javacodegeeks.snippets.enterprise.services;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    @SuppressWarnings("resource")
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        World hello = (World) context.getBean("helloWorldBean");
        hello.sayHello();
        hello.sayGoodbye();
    }
}
