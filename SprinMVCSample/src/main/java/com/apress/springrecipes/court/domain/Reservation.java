package com.apress.springrecipes.court.domain;

import java.util.Date;

public class Reservation {

    private String courtName;
    private Date date;
    private int hour;
    private Player player;
    private SportType sportType;

    public String getCourtName() {
        return this.courtName;
    }

    public Date getDate() {
        return this.date;
    }

    public int getHour() {
        return this.hour;
    }

    public Player getPlayer() {
        return this.player;
    }

    public SportType getSportType() {
        return this.sportType;
    }

    public void setCourtName(String courtName) {
        this.courtName = courtName;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setSportType(SportType sportType) {
        this.sportType = sportType;
    }

}
