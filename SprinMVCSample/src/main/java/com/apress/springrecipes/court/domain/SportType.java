package com.apress.springrecipes.court.domain;

public class SportType {

    private int id;
    private String name;

    public SportType(int i, String string) {
        this.id = i;
        this.name = string;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

}
